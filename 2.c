#include <stdio.h>
int main() {
  int x, y, z = 0;
  printf("Enter a integer: ");
  scanf("%d", &x);

  for (y = 2; y <= x / 2; ++y) {
    if (x % y == 0) {
      z = 1;
      break;
    }
  }

  if (x == 1) {
    printf("1 is not a prime number");
  } 
  else {
    if (z == 0)
      printf("%d is a prime number.", x);
    else
      printf("%d isn't a prime number.", x);
  }

  return 0;
}
